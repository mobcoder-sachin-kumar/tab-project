/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Country from './Source/Country';
import Questions from './Source/Questions';

AppRegistry.registerComponent(appName, () => Questions);
