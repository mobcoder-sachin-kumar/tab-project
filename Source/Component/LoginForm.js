import React, {useState, useEffect, useRef} from 'react'
import { Dimensions, StyleSheet, Text, View, Animated } from 'react-native'
import CustomTextInput from './CustomTextInput'


const width = Dimensions.get('window').width;
const LoginForm = () => {
    const [isEmailFocused, setIsEmailFocused] = useState(false);
    const [isPasswordFocused, setIsPasswordFocused] = useState(false);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const emailRef = useRef(null);
    const passwordRef = useRef();

    useEffect(() => {
        if (email.length <= 0) {
          setIsEmailFocused(false);
        } else {
          setIsEmailFocused(true);
        }
      }, [email]);

      useEffect(() => {
        if (password.length <= 0) {
          setIsPasswordFocused(false);
        } else {
          setIsPasswordFocused(true);
        }
      }, [password]);
    

  return (
    <View>
      <View style = {styles.textInputView} >
      {isEmailFocused && (
          <Animated.View style={styles.animatedStyle}>
            <Text style={{fontSize: 12}}>Enter Email address</Text>
          </Animated.View>
        )}
        <CustomTextInput
           placeholder={isEmailFocused ? '' : 'Enter Email address'}
           placeholderTextColor="black"
           keyboardType="email-address"
           returnKeyType = 'next'
           onChangeText={text => setEmail(text)}
           refs = {emailRef}
           onSubmitEditing = {()=>passwordRef.current.focus()}
        />
      </View>

      <View style = {styles.textInputView} >
      {isPasswordFocused && (
          <Animated.View style={styles.animatedStyle}>
            <Text style={{fontSize: 12}}>Enter Password</Text>
          </Animated.View>
        )}
        <CustomTextInput
           placeholder={isPasswordFocused ? '' : 'Enter Password'}
           placeholderTextColor="black"
           keyboardType="email-address"
           returnKeyType = 'next'
           onChangeText={text => setPassword(text)}
           refs = {passwordRef}
        //    onSubmitEditing = {()=>.current.focus()}
        />
      </View>
    </View>
  )
}

export default LoginForm

const styles = StyleSheet.create({

    textInputView : {
        height : 50,
        width : width - 90,
        marginVertical : 20,
        justifyContent : "center",
        borderWidth : 1,
        borderRadius : 7,
        backgroundColor : "#e0e0eb",
        borderColor : "white"
    },
    animatedStyle: {
        top : 0,
        left: 5,
        position: 'relative',
        borderRadius: 90,
        zIndex: 10000,
    },
})