import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'

const YesNoButton = (props) => {
  return (
    <View>
      <TouchableOpacity 
        style = {[styles.button, {backgroundColor : props.backgroundColor}, {position : props.position}]} 
        onPress = {props.onPress}
        value = {props.value}
      >
          <Text style={{color : props.color}} >{props.title}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default YesNoButton

const styles = StyleSheet.create({
 button :
 { 
     height : 35,
     width : 80, 
     borderWidth : 1,
     borderRadius : 15,
     justifyContent : "center",
     alignItems : "center",
     borderColor : "white",
     marginRight : 20
  },
})