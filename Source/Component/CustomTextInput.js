import { Dimensions, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useRef } from 'react'

const width = Dimensions.get('window').width;
const CustomTextInput = props => {
  return (
    <View>
      <TextInput
       placeholder = {props.placeholder}
       placeholderTextColor = {props.placeholderTextColor}
       onFocus={props.onFocus}
       keyboardType = {props.keyboardType}
       returnKeyType = {props.returnKeyType}
       ref = {props.refs}
       onSubmitEditing = {props.onSubmitEditing}
       onChangeText = {props.onChangeText}
       style = {[styles.txtInput, {width : props.width, 
        borderTopRightRadius : props.borderTopRightRadius, 
        borderBottomRightRadius : props.borderBottomRightRadius,
        height : props.height, borderWidth : props.borderWidth,
        flex : props.flex, borderColor : props.borderColor,
        backgroundColor : props.backgroundColor, fontSize : props.fontSize
      }]}
       editable = {props.editable}
       onFocus = {props.onFocus}
      />
    </View>
  )
}

export default CustomTextInput

const styles = StyleSheet.create({
  txtInput : {
    fontSize : 14,
    padding : 10,
    height : 40, 
    width : width - 20,
    borderRadius : 10,
    borderColor : "white",
    backgroundColor : "#f0f0f5",
    marginVertical : 10,
  }
})