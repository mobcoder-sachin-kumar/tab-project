import { Dimensions, FlatList, Image, Modal, SafeAreaView, StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import CustomTextInput from './Component/CustomTextInput'


const width = Dimensions.get('window').width;
const ModalView = (props) => {
    console.log(props)
    const [data, setData] = useState()
    const [filteredDataSource, setFilteredDataSource] = useState()
    const [isVisible, setIsVisible] = useState()
    useEffect(() => {
        searchCountry()
        console.log(filteredDataSource)
        setData(props.data)
        setIsVisible(props.visible)
        console.log(isVisible)
    }, [isVisible])

    const renderList = ({item, index}) => {
        return (
            <TouchableOpacity style = {[styles.cardView, { borderColor : index%2 == 0? "#9a9a9a": "#ededed" }]} >
                <Text>{item.value}</Text>
            </TouchableOpacity>
        )
    }
    const searchCountry = (text) => {
        console.warn(text)
        if (text) {
          let value = data.filter(function (data, index) {
            let searchstring = data.value;
            return searchstring.toUpperCase().indexOf(text.toUpperCase()) != -1;
          });
          console.warn(value)
          setFilteredDataSource(value);
        } else {
          setFilteredDataSource(data);
        //   setSearch(text)
        }
      };
    
  return (
    // <SafeAreaView style = {styles.container} >
        // <View style={styles.container} >
        <Modal
             animationType = {props.animationType}
             transparent={props.transparent}
             visible={props.visible}
             onRequestClose={props.onRequestClose}
             style = {styles.modalStyle}
        >
            <SafeAreaView style={styles.container} >
                <View style={{ flex : 0.2, width : width - 20, }} >
                    <View style = {{flex : 0.5, flexDirection : "row", alignItems : "center", justifyContent : "space-between",}} >
                        <Text style = {{fontSize : 20}} >Country</Text>
                        <TouchableOpacity
                            onPress={()=>setIsVisible(false)}
                        >
                        <Image source={require('./assets/close.png')} style={{ height : 20, width : 20 }} />
                        </TouchableOpacity>
                    </View>
                    <View style = {{flex : 0.5,}} >
                        <CustomTextInput
                            width = {width - 30}
                            height = {50}
                            placeholder = {"Search"}
                            placeholderTextColor = {"#ababab"}
                            fontSize = {16}
                            borderWidth = {1}
                            borderColor = {"black"}
                            backgroundColor  = {"#ffffff"}
                            onChangeText = {(text)=>searchCountry(text)}
                        />
                    </View>
                </View>
                <View style={{ flex : 0.8, width : "100%",  }} >
                            <FlatList 
                                data = {filteredDataSource}
                                renderItem = {renderList}
                                // keyExtractor = {()=> console.log(index)}
                            />
                </View>
            </SafeAreaView>
        </Modal>
        // </View>
    // </SafeAreaView>
  )
}

export default ModalView

const styles = StyleSheet.create({
    container : {
        flex : 1,
        alignItems : "center",
        borderWidth : 1
    },
    modalStyle : {
        flex : 1,
        width : "100%",
        borderWidth : 1,
    },
    cardView : {
       borderBottomWidth : 0.5,
       borderColor : "#9a9a9a",
       height : 50,
       padding : 10,
       justifyContent : "center",
    }
})