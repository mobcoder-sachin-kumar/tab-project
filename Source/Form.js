import React, {useState, useEffect, useRef} from 'react';
import {Animated, StyleSheet, Text, View, TouchableWithoutFeedback, Keyboard} from 'react-native';
import CustomTextInput from './Component/CustomTextInput';

const Form = () => {
  const [email, setEmail] = useState('');
  const [number, setNumber] = useState('');
  const [isEmailFocused, setisEmailFocused] = useState(false);
  const [isNumberFocused, setisNumberFocused] = useState(false);
  const emailRef = useRef(null);
  const numberRef = useRef();

  useEffect(()=>{
    console.log(emailRef.current.focus())
  },[])
  useEffect(() => {
    if (email.length <= 0) {
      setisEmailFocused(false);
    } else {
      setisEmailFocused(true);
    }
  }, [email]);

  useEffect(() => {
    if (number.length <= 0) {
      setisNumberFocused(false);
    } else {
      setisNumberFocused(true);
    }
  }, [number]);

  return (
    <View
      style={styles.container}>
      <TouchableWithoutFeedback
        onPress={() => Keyboard.dismiss()}
      >
     <View
      style={styles.container}>
      <View style={styles.textFieldView}>
        {isEmailFocused && (
          <Animated.View style={styles.animatedStyle}>
            <Text style={{fontSize: 12}}>Enter Email</Text>
          </Animated.View>
        )}
        <CustomTextInput
          placeholder={isEmailFocused ? '' : 'Enter Email'}
          placeholderTextColor="black"
          keyboardType="email-address"
          returnKeyType = 'next'
          refs = {emailRef}
          // onFocus = {()=>setisEmailFocused(true)}
          onChangeText={text => setEmail(text)}
          onSubmitEditing = {()=>numberRef.current.focus()}
        />
      </View>
      <View style={styles.textFieldView}>
        {isNumberFocused && (
          <Animated.View style={styles.animatedStyle}>
            <Text style={{fontSize: 12}}>Enter Number</Text>
          </Animated.View>
        )}
        <CustomTextInput
          placeholder={isNumberFocused ? '' : 'Enter Number'}
          placeholderTextColor="black"
          keyboardType="numeric"
          returnKeyType = 'done'
          refs = {numberRef}
          onChangeText={text => setNumber(text)}
        />
      </View>
      </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default Form;

const styles = StyleSheet.create({
    container : {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textFieldView: {
    height: 40,
    width: 200,
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    marginVertical: 20,
    },
    animatedStyle: {
    top : 0,
    left: 5,
    position: 'relative',
    borderRadius: 90,
    zIndex: 10000,
    },
});
