import {
  Dimensions,
  FlatList,
  Image,
  Modal,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import data from './assets/data.json';
import YesNoButton from './Component/YesNoButton';
import CustomTextInput from './Component/CustomTextInput';
import KeyboardAvoidingView from 'react-native/Libraries/Components/Keyboard/KeyboardAvoidingView';
import ModalView from './ModalView';
import CountryInput from './CountryInput';
import countryList from './Country.json';

const width = Dimensions.get('window').width;
const Questions = () => {
  const [selectedId, setSelectedId] = useState();
  const [selectedOptions, setSelectedoptions] = useState([]);
  const [isChecked, setIsChecked] = useState('');
  const [isFocused, setIsFocused] = useState(false);
  const [filteredDataSource, setFilteredDataSource] = useState();
  const [countryData, setCountryData] = useState();
  const [selectedCountry, setSelectedCountry] = useState('Please Select');
  const [selectedCountryId, setSelectedCountryId] = useState('');

  useEffect(() => {
    setCountryData(countryList);
    searchCountry();
  }, [isFocused]);

  useEffect(() => {}, [selectedCountry, selectedCountryId]);

  const openOptions = (id, index) => {
    setSelectedId(id);
    const newArray = [...selectedOptions];
    newArray[index] = true;
    setSelectedoptions(newArray);
  };

  const closeOptions = (index, id) => {
    const newArray = [...selectedOptions];
    newArray[index] = false;
    setSelectedoptions(newArray);
    console.log(selectedOptions);
    setSelectedId(null);
  };

  const checkBox = id => {
    setIsChecked(id);
  };

  const focusChange = () => {
    setIsFocused(true);
    console.log(isFocused);
  };
  const renderItem = ({item, index}) => {
    return (
      <View>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              marginRight: 3,
              fontSize: 14,
              color: '#7f7f7f',
            }}>
            {index + 1}.
          </Text>
          <Text style={{flexShrink: 1, color: '#7f7f7f'}}>
            {item['question']}
          </Text>
        </View>
        <View style={{flexDirection: 'row', marginVertical: 30, width: '100%'}}>
          <YesNoButton
            title="Yes"
            onPress={() => openOptions(item.id, index)}
            value={'yes'}
            backgroundColor={selectedOptions[index] ? '#2da5fe' : '#e5e5f3'}
            color={selectedOptions[index] ? 'white' : '#858585'}
          />
          <YesNoButton
            title={item['options'][0]['value']}
            backgroundColor={
              selectedOptions[index] == false
                ? '#2da5fe'
                : selectedOptions[index] == undefined
                ? '#e5e5f3'
                : selectedOptions[index] == true
                ? '#e5e5f3'
                : null
            }
            color={selectedOptions[index] == false ? 'white' : '#858585'}
            onPress={() => closeOptions(index, item.id)}
          />
        </View>
        {selectedId == item.id && (
          <View style={{width: '100%'}}>
            <FlatList
              data={item['options']}
              renderItem={renderOptions}
              style={{marginBottom: 10}}
            />
          </View>
        )}
      </View>
    );
  };

  const renderOptions = ({item, index}) => {
    if (index != 0) {
      if (item.id > 7) {
        return (
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 10,
              }}
              onPress={() => checkBox(item.id)}>
              <TouchableOpacity
                style={[
                  styles.checkBox,
                  {
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}
                onPress={() => checkBox(item.id)}>
                <View
                  style={{
                    backgroundColor: isChecked == item.id ? '#2da5fe' : 'white',
                    height: 10,
                    width: 10,
                    borderRadius: 10,
                  }}></View>
              </TouchableOpacity>
              <Text
                style={{
                  marginLeft: 10,
                  fontWeight: item.id == isChecked ? 'bold' : 'normal',
                  color: 'black',
                }}>
                {item.value}
              </Text>
            </TouchableOpacity>
            {isChecked == item.id && (
              <FlatList data={item.subOption} renderItem={renderSubOptions} />
            )}
          </View>
        );
      } else if (
        item.value == 'Country of the main financial deals or businesses' ||
        item.value == 'Country of Employment'
      ) {
        return (
          <View>
            <Text style={{marginVertical: 10}}>{item.value}</Text>
            <CountryInput
              onPress={() => focusChange()}
              placeholder={selectedCountry}
            />
          </View>
        );
      } else {
        return (
          <View>
            <Text style={{marginVertical: 10}}>{item.value}</Text>
            <CustomTextInput
              placeholder={item.value}
              placeholderTextColor={'#a3a3c2'}
              onFocus={() => focusChange()}
              borderWidth={1}
              borderColor={'white'}
              backgroundColor={'#e5e5f3'}
            />
          </View>
        );
      }
    }
  };

  const renderSubOptions = ({item}) => {
    if (item.value == 'Country of Employment') {
      return (
        <View>
          <Text style={{marginVertical: 10}}>{item.value}</Text>
          <CountryInput
            onPress={() => focusChange()}
            placeholder={selectedCountry}
            // backgroundColor = {"#e5e5f3"}
          />
        </View>
      );
    } else {
      return (
        <View>
          <Text style={{marginVertical: 10}}>{item.value}</Text>
          <CustomTextInput
            placeholder={item.value}
            placeholderTextColor={'#a3a3c2'}
            backgroundColor={'#e5e5f3'}
          />
        </View>
      );
    }
  };

  const listFooter = () => {
    return (
      <View>
        <TouchableOpacity style={styles.submit}>
          <Text style={{fontSize: 16, color: 'white'}}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderList = ({item, index}) => {
    return (
      <TouchableOpacity
        style={[
          styles.cardView,
          {borderColor: index % 2 == 0 ? '#9a9a9a' : '#ededed'},
        ]}
        onPress={() => setCountry(item)}>
        <Text>{item.value}</Text>
      </TouchableOpacity>
    );
  };

  const searchCountry = text => {
    if (text) {
      let value = countryData.filter(function (data, index) {
        let searchstring = data.value;
        return searchstring.toUpperCase().indexOf(text.toUpperCase()) != -1;
      });
      setFilteredDataSource(value);
    } else {
      setFilteredDataSource(countryData);
    }
  };

  const setCountry = item => {
    setSelectedCountry(item.value);
    setSelectedCountryId(item.id);
    setIsFocused(false);
  };
  return (
    <SafeAreaView style={styles.container}>
      <Modal visible={isFocused} style={styles.modalStyle}>
        <SafeAreaView style={styles.container}>
          <View style={{flex: 0.2, width: width - 20}}>
            <View
              style={{
                flex: 0.5,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 20}}>Country</Text>
              <TouchableOpacity onPress={() => setIsFocused(false)}>
                <Image
                  source={require('./assets/close.png')}
                  style={{height: 20, width: 20}}
                />
              </TouchableOpacity>
            </View>
            <View style={{flex: 0.5, alignItems: 'center'}}>
              <CustomTextInput
                width={width - 30}
                height={50}
                placeholder={'Search'}
                placeholderTextColor={'#ababab'}
                fontSize={16}
                borderWidth={1}
                borderColor={'black'}
                backgroundColor={'#ffffff'}
                onChangeText={text => searchCountry(text)}
              />
            </View>
          </View>
          <View style={{flex: 0.8, width: '100%'}}>
            <FlatList
              data={filteredDataSource}
              renderItem={renderList}
              // keyExtractor = {()=> console.log(index)}
            />
          </View>
        </SafeAreaView>
      </Modal>
      <View style={styles.header}>
        <Text style={styles.headingMain}>Update Information</Text>
        <Text style={styles.stepsStyle}>Step 2 of 2</Text>
      </View>
      <KeyboardAvoidingView
        style={styles.questionView}
        behavior={Platform.OS == 'ios' ? 'padding' : ''}>
        <View style={{flex: 1, marginTop: 10}}>
          <FlatList
            data={data}
            renderItem={renderItem}
            showsVerticalScrollIndicator={false}
            ListFooterComponent={listFooter}
          />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Questions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  header: {
    flex: 0.1,
    width: width - 20,
    justifyContent: 'center',
  },
  headingMain: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#0d0f52',
  },
  stepsStyle: {
    color: '#7f7f7f',
    marginTop: 10,
  },
  questionView: {
    flex: 0.9,
    width: width - 20,
  },
  checkBox: {
    height: 15,
    borderWidth: 0.5,
    width: 15,
    borderRadius: 15,
  },
  submit: {
    backgroundColor: '#3399ff',
    height: 50,
    width: width - 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#3399ff',
    alignSelf: 'center',
    // position : "relative"
  },
  countryPicker: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    alignItems: 'center',
    width: width - 20,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 7,
    backgroundColor: '#f0f0f5',
  },
  modalStyle: {
    flex: 1,
    borderColor: 'red',
    borderWidth: 1,
    width: '100%',
    borderWidth: 1,
  },
  cardView: {
    borderBottomWidth: 0.5,
    borderColor: '#9a9a9a',
    height: 50,
    padding: 10,
    justifyContent: 'center',
  },
});
