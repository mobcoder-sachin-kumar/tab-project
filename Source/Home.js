import React, {useState, useEffect, useRef} from 'react'
import { View, Text, ScrollView, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import About from './About'
import LoginForm from './Component/LoginForm'
import ContactUs from './ContactUs'

const width = Dimensions.get('window').width
const Home = () => {

return (
  <View style = {styles.container} >
  <View style={{flex : 0.4, justifyContent : "center", borderWidth : 1, borderColor : 'red'}} >
    <LoginForm/>
  </View>
  </View>
)
}

export default Home

const styles = StyleSheet.create({
  container : {
    flex : 1,
    width : "100%",
    justifyContent : "center",
    alignItems : "center",
    borderWidth : 1,
    borderColor : "black", 
  },
})