import { Image, StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native'
import React from 'react'
import CustomTextInput from './Component/CustomTextInput'

const width = Dimensions.get('window').width;
const CountryInput = (props) => {
  return (
    <View>
      <TouchableOpacity
              onPress={props.onPress}
              style={styles.countryPicker}
            >
            <CustomTextInput
              placeholder={props.placeholder}
              placeholderTextColor={'#a3a3c2'}
              onFocus = {()=>focusChange()}
              editable = {false}
              flex = {0.8}
              width = {"100%"}
              height = {"100%"}
              borderWidth = {0}
              borderBottomRightRadius = {0}
              borderTopRightRadius = {0}
            />
            <View style = {{flex : 0.2,height : "100%", justifyContent : "center", alignItems : "center", }} >
              <Image source={require('./assets/down.png')} style={{ height : 20, width : 20}} />
            </View>
            </TouchableOpacity>
    </View>
  )
}

export default CountryInput

const styles = StyleSheet.create({
    countryPicker : { 
        flex : 1,
        flexDirection : "row", 
        justifyContent : "space-between",
        height : 40, 
        alignItems : "center", 
        width : width-20, 
        borderColor : 'white', 
        borderWidth : 1, 
        borderRadius : 10, 
        backgroundColor : "#e5e5f3" 
      }
})