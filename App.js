import { View, Text, SafeAreaView, StyleSheet, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import React, {useState, useEffect, useRef} from 'react'
import Home from './Source/Home'
import About from './Source/About'
import ContactUs from './Source/ContactUs'
import Questions from './Source/Questions'

const width = Dimensions.get('window').width
const App = () => {
  const [selectedButton, setSelectedButton] = useState('1')
  const buttons = [
    {
      id : "1",
      title : "Home"
    },
    {
      id : "2",
      title : "About us"
    },
    {
      id : "3",
      title : "Contact us"
    },
    {
      id : "4",
      title : "Schedule"
    },
    {
      id : '5',
      title : "Questions"
    }
  ]
  const scrollRef = useRef()
  
 const openView = (item) => {
    setSelectedButton(item.id)
    switch(item.id){
      case "1": return scrollRef.current.scrollTo({x : 0, animated : true});
      case "2": return scrollRef.current.scrollTo({x : 150, animated : true});
      case "3": return scrollRef.current.scrollTo({x : 330, animated : true});
      case "4": return scrollRef.current.scrollTo({x : 540, animated : true});
      case '5': return scrollRef.current.scrollTo({x : 720, animated : true})  
    }
  }
  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex : 0.05}} >
        <ScrollView horizontal = {true} showsHorizontalScrollIndicator = {false} ref = {scrollRef}   >
        {buttons.map((item)=>
          <TouchableOpacity
          style = {[styles.tabs, {backgroundColor : item.id == selectedButton ? "black": "#6666ff" }]}
          onPress = {()=>openView(item)}

         >
           <Text style={styles.tabTxt} >{item.title}</Text>
         </TouchableOpacity> 
        )}
        </ScrollView>
        </View>
        <View style={{flex : 0.95, alignItems : "center"}} >
        {selectedButton == "1" && <Home/>}
        {selectedButton == "2" && <About/>}
        {selectedButton == "3" && <ContactUs/>}
        {selectedButton == "5" && <Questions/>}
        </View>
    </SafeAreaView>
  )
}


export default App

const styles = StyleSheet.create({
  container : {
    flex : 1,
  },
  tabs : {
    height : 40, 
    width : width/2-15, 
    borderWidth : 0.5,
    justifyContent : "center",
    alignItems : "center",
    borderRadius : 7, 
    borderColor : "black", 
    alignSelf : "center",
  },
  tabTxt : {
    fontSize : 26,
    fontWeight : "bold",
    color : "white"
  }
})